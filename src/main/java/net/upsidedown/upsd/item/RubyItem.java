
package net.upsidedown.upsd.item;

import net.upsidedown.upsd.init.UpsdModTabs;

import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Item;

public class RubyItem extends Item {
	public RubyItem() {
		super(new Item.Properties().tab(UpsdModTabs.TAB_UPSIDE_DOWN_ITEM).stacksTo(64).rarity(Rarity.UNCOMMON));
		setRegistryName("ruby");
	}

	@Override
	public int getUseDuration(ItemStack itemstack) {
		return 0;
	}
}
