
package net.upsidedown.upsd.item;

import net.upsidedown.upsd.init.UpsdModTabs;

import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Item;

public class DarkGlowstoneDustItem extends Item {
	public DarkGlowstoneDustItem() {
		super(new Item.Properties().tab(UpsdModTabs.TAB_UPSIDE_DOWN_ITEM).stacksTo(64).rarity(Rarity.RARE));
		setRegistryName("dark_glowstone_dust");
	}

	@Override
	public int getUseDuration(ItemStack itemstack) {
		return 0;
	}
}
