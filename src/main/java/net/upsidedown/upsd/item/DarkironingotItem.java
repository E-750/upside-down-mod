
package net.upsidedown.upsd.item;

import net.upsidedown.upsd.init.UpsdModTabs;

import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Item;

public class DarkironingotItem extends Item {
	public DarkironingotItem() {
		super(new Item.Properties().tab(UpsdModTabs.TAB_UPSIDE_DOWN_ITEM).stacksTo(64).rarity(Rarity.RARE));
		setRegistryName("darkironingot");
	}

	@Override
	public int getUseDuration(ItemStack itemstack) {
		return 0;
	}
}
