
package net.upsidedown.upsd.item;

import net.upsidedown.upsd.init.UpsdModTabs;
import net.upsidedown.upsd.init.UpsdModItems;

import net.minecraftforge.registries.ForgeRegistries;

import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.Entity;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.resources.ResourceLocation;

public abstract class DarkIronArmorItem extends ArmorItem {
	public DarkIronArmorItem(EquipmentSlot slot, Item.Properties properties) {
		super(new ArmorMaterial() {
			@Override
			public int getDurabilityForSlot(EquipmentSlot slot) {
				return new int[]{13, 15, 16, 11}[slot.getIndex()] * 25;
			}

			@Override
			public int getDefenseForSlot(EquipmentSlot slot) {
				return new int[]{4, 7, 7, 4}[slot.getIndex()];
			}

			@Override
			public int getEnchantmentValue() {
				return 9;
			}

			@Override
			public SoundEvent getEquipSound() {
				return ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation("item.armor.equip_generic"));
			}

			@Override
			public Ingredient getRepairIngredient() {
				return Ingredient.of(new ItemStack(UpsdModItems.DARKIRONINGOT));
			}

			@Override
			public String getName() {
				return "dark_iron_armor";
			}

			@Override
			public float getToughness() {
				return 2f;
			}

			@Override
			public float getKnockbackResistance() {
				return 0.4f;
			}
		}, slot, properties);
	}

	public static class Helmet extends DarkIronArmorItem {
		public Helmet() {
			super(EquipmentSlot.HEAD, new Item.Properties().tab(UpsdModTabs.TAB_UPSIDE_DOWN_ARMOR));
			setRegistryName("dark_iron_armor_helmet");
		}

		@Override
		public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
			return "upsd:textures/models/armor/darkironarmor_layer_1.png";
		}
	}

	public static class Chestplate extends DarkIronArmorItem {
		public Chestplate() {
			super(EquipmentSlot.CHEST, new Item.Properties().tab(UpsdModTabs.TAB_UPSIDE_DOWN_ARMOR));
			setRegistryName("dark_iron_armor_chestplate");
		}

		@Override
		public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
			return "upsd:textures/models/armor/darkironarmor_layer_1.png";
		}
	}

	public static class Leggings extends DarkIronArmorItem {
		public Leggings() {
			super(EquipmentSlot.LEGS, new Item.Properties().tab(UpsdModTabs.TAB_UPSIDE_DOWN_ARMOR));
			setRegistryName("dark_iron_armor_leggings");
		}

		@Override
		public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
			return "upsd:textures/models/armor/darkironarmor_layer_2.png";
		}
	}

	public static class Boots extends DarkIronArmorItem {
		public Boots() {
			super(EquipmentSlot.FEET, new Item.Properties().tab(UpsdModTabs.TAB_UPSIDE_DOWN_ARMOR));
			setRegistryName("dark_iron_armor_boots");
		}

		@Override
		public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
			return "upsd:textures/models/armor/darkironarmor_layer_1.png";
		}
	}
}
