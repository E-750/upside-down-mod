package net.upsidedown.upsd.client.renderer;

import net.upsidedown.upsd.entity.HrydaEntity;
import net.upsidedown.upsd.client.model.Modelcustom_model;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.entity.layers.EyesLayer;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.RenderType;

public class HrydaRenderer extends MobRenderer<HrydaEntity, Modelcustom_model<HrydaEntity>> {
	public HrydaRenderer(EntityRendererProvider.Context context) {
		super(context, new Modelcustom_model(context.bakeLayer(Modelcustom_model.LAYER_LOCATION)), 0.5f);
		this.addLayer(new EyesLayer<HrydaEntity, Modelcustom_model<HrydaEntity>>(this) {
			@Override
			public RenderType renderType() {
				return RenderType.eyes(new ResourceLocation("upsd:textures/hydra.png"));
			}
		});
	}

	@Override
	public ResourceLocation getTextureLocation(HrydaEntity entity) {
		return new ResourceLocation("upsd:textures/hydra.png");
	}
}
