
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.upsidedown.upsd.init;

import net.upsidedown.upsd.item.UpsideDownDimItem;
import net.upsidedown.upsd.item.RubyItem;
import net.upsidedown.upsd.item.DarkironingotItem;
import net.upsidedown.upsd.item.DarkIronArmorItem;
import net.upsidedown.upsd.item.DarkGlowstoneDustItem;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.RegistryEvent;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.item.SpawnEggItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.BlockItem;

import java.util.List;
import java.util.ArrayList;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class UpsdModItems {
	private static final List<Item> REGISTRY = new ArrayList<>();
	public static final Item DARK_IRON_ORE = register(UpsdModBlocks.DARK_IRON_ORE, UpsdModTabs.TAB_UPSIDE_DOWN_BLOCK);
	public static final Item UPSIDEDOWNDIRT = register(UpsdModBlocks.UPSIDEDOWNDIRT, UpsdModTabs.TAB_UPSIDE_DOWN_BLOCK);
	public static final Item UPSIDEDOWNGRASS = register(UpsdModBlocks.UPSIDEDOWNGRASS, UpsdModTabs.TAB_UPSIDE_DOWN_BLOCK);
	public static final Item UPSIDEDOWNSTONE = register(UpsdModBlocks.UPSIDEDOWNSTONE, UpsdModTabs.TAB_UPSIDE_DOWN_BLOCK);
	public static final Item RUBYORE = register(UpsdModBlocks.RUBYORE, UpsdModTabs.TAB_UPSIDE_DOWN_BLOCK);
	public static final Item RUBY = register(new RubyItem());
	public static final Item DARKIRONINGOT = register(new DarkironingotItem());
	public static final Item DARK_GLOWSTONE_DUST = register(new DarkGlowstoneDustItem());
	public static final Item DARK_IRON_ARMOR_HELMET = register(new DarkIronArmorItem.Helmet());
	public static final Item DARK_IRON_ARMOR_CHESTPLATE = register(new DarkIronArmorItem.Chestplate());
	public static final Item DARK_IRON_ARMOR_LEGGINGS = register(new DarkIronArmorItem.Leggings());
	public static final Item DARK_IRON_ARMOR_BOOTS = register(new DarkIronArmorItem.Boots());
	public static final Item UPSIDE_DOWN_DIM = register(new UpsideDownDimItem());
	public static final Item HRYDA = register(
			new SpawnEggItem(UpsdModEntities.HRYDA, -13434829, -10092442, new Item.Properties().tab(UpsdModTabs.TAB_UPSIDE_DOWN_EGGS))
					.setRegistryName("hryda_spawn_egg"));

	private static Item register(Item item) {
		REGISTRY.add(item);
		return item;
	}

	private static Item register(Block block, CreativeModeTab tab) {
		return register(new BlockItem(block, new Item.Properties().tab(tab)).setRegistryName(block.getRegistryName()));
	}

	@SubscribeEvent
	public static void registerItems(RegistryEvent.Register<Item> event) {
		event.getRegistry().registerAll(REGISTRY.toArray(new Item[0]));
	}
}
