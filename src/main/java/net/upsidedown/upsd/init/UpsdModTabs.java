
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.upsidedown.upsd.init;

import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.world.item.Items;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.CreativeModeTab;

public class UpsdModTabs {
	public static CreativeModeTab TAB_UPSIDE_DOWN_ARMOR;
	public static CreativeModeTab TAB_UPSIDE_DOWN_BLOCK;
	public static CreativeModeTab TAB_UPSIDE_DOWN_EGGS;
	public static CreativeModeTab TAB_UPSIDE_DOWN_ITEM;
	public static CreativeModeTab TAB_UPSIDE_DOWN_TOOLS;

	public static void load() {
		TAB_UPSIDE_DOWN_ARMOR = new CreativeModeTab("tabupside_down_armor") {
			@Override
			public ItemStack makeIcon() {
				return new ItemStack(Items.NETHERITE_CHESTPLATE);
			}

			@OnlyIn(Dist.CLIENT)
			public boolean hasSearchBar() {
				return false;
			}
		};
		TAB_UPSIDE_DOWN_BLOCK = new CreativeModeTab("tabupside_down_block") {
			@Override
			public ItemStack makeIcon() {
				return new ItemStack(UpsdModBlocks.DARK_IRON_ORE);
			}

			@OnlyIn(Dist.CLIENT)
			public boolean hasSearchBar() {
				return false;
			}
		};
		TAB_UPSIDE_DOWN_EGGS = new CreativeModeTab("tabupside_down_eggs") {
			@Override
			public ItemStack makeIcon() {
				return new ItemStack(Items.SALMON_SPAWN_EGG);
			}

			@OnlyIn(Dist.CLIENT)
			public boolean hasSearchBar() {
				return false;
			}
		};
		TAB_UPSIDE_DOWN_ITEM = new CreativeModeTab("tabupside_down_item") {
			@Override
			public ItemStack makeIcon() {
				return new ItemStack(Items.CLAY_BALL);
			}

			@OnlyIn(Dist.CLIENT)
			public boolean hasSearchBar() {
				return false;
			}
		};
		TAB_UPSIDE_DOWN_TOOLS = new CreativeModeTab("tabupside_down_tools") {
			@Override
			public ItemStack makeIcon() {
				return new ItemStack(Items.NETHERITE_PICKAXE);
			}

			@OnlyIn(Dist.CLIENT)
			public boolean hasSearchBar() {
				return false;
			}
		};
	}
}
