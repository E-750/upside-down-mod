
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.upsidedown.upsd.init;

import net.upsidedown.upsd.client.renderer.HrydaRenderer;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.api.distmarker.Dist;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class UpsdModEntityRenderers {
	@SubscribeEvent
	public static void registerEntityRenderers(EntityRenderersEvent.RegisterRenderers event) {
		event.registerEntityRenderer(UpsdModEntities.HRYDA, HrydaRenderer::new);
	}
}
