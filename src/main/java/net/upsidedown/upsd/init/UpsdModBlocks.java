
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.upsidedown.upsd.init;

import net.upsidedown.upsd.block.UpsidedownstoneBlock;
import net.upsidedown.upsd.block.UpsidedowngrassBlock;
import net.upsidedown.upsd.block.UpsidedowndirtBlock;
import net.upsidedown.upsd.block.UpsideDownDimPortalBlock;
import net.upsidedown.upsd.block.RubyoreBlock;
import net.upsidedown.upsd.block.DarkIronOreBlock;

import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.world.level.block.Block;

import java.util.List;
import java.util.ArrayList;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class UpsdModBlocks {
	private static final List<Block> REGISTRY = new ArrayList<>();
	public static final Block DARK_IRON_ORE = register(new DarkIronOreBlock());
	public static final Block UPSIDEDOWNDIRT = register(new UpsidedowndirtBlock());
	public static final Block UPSIDEDOWNGRASS = register(new UpsidedowngrassBlock());
	public static final Block UPSIDEDOWNSTONE = register(new UpsidedownstoneBlock());
	public static final Block RUBYORE = register(new RubyoreBlock());
	public static final Block UPSIDE_DOWN_DIM_PORTAL = register(new UpsideDownDimPortalBlock());

	private static Block register(Block block) {
		REGISTRY.add(block);
		return block;
	}

	@SubscribeEvent
	public static void registerBlocks(RegistryEvent.Register<Block> event) {
		event.getRegistry().registerAll(REGISTRY.toArray(new Block[0]));
	}

	@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
	public static class ClientSideHandler {
		@SubscribeEvent
		public static void clientSetup(FMLClientSetupEvent event) {
			UpsidedowngrassBlock.registerRenderLayer();
		}
	}
}
